package care.masana.common

import java.util.UUID

case class ProfessionalRef(uuid: UUID) extends IdentityRef
