package care.masana.common

import java.util.UUID

/**
 * This is just a way to share valid refs between all modules. You are not
 * expected to use or modify this file.
 */
object Fixtures {

  val firstProfessionalRef = ProfessionalRef(UUID.randomUUID())
  val secondProfessionalRef = ProfessionalRef(UUID.randomUUID())

  val firstProfessionRef = ProfessionRef(UUID.randomUUID())
  val secondProfessionRef = ProfessionRef(UUID.randomUUID())
  val thirdProfessionRef = ProfessionRef(UUID.randomUUID())

}
