package care.masana.common

import java.util.UUID

case class BeneficiaryRef(uuid: UUID) extends IdentityRef
