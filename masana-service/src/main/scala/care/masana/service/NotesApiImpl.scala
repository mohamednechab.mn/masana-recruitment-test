package care.masana.service

import care.masana.common.ProfessionalRef
import care.masana.notes.Note
import care.masana.professionals.ProfessionalRepository

class NotesApiImpl {

  /**
   * @param professional The reference of the identified professional.
   * @return a collection of notes that should be read by the professional.
   */
  def getNotesFor(
      professional: ProfessionalRef
  ): Response[Seq[Note]] = ???

}
