package care.masana.professionals

import care.masana.common.{ProfessionRef, ProfessionalRef}

case class Professional(
    ref: ProfessionalRef,
    professions: Seq[ProfessionRef]
)
