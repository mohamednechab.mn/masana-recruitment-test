package care.masana.notes

import care.masana.common.{
  BeneficiaryRef,
  Fixtures,
  NoteRef,
  ProfessionRef,
  ProfessionalRef
}

import java.time.LocalDateTime
import java.util.UUID

class NotesRepository {}

object NotesRepository {
  /* Assume this collection came from the persistence layer.
   * You can use it at your will
   */
  private[notes] val notes: Seq[Note] = {
    val beneficiaryRef = BeneficiaryRef(UUID.randomUUID())
    val template = Note(
      NoteRef(UUID.randomUUID()),
      beneficiaryRef,
      beneficiaryRef,
      LocalDateTime.now(),
      important = false,
      Seq.empty
    )
    Iterable
      .tabulate(5) {
        case i if i < 3 =>
          template.copy(
            dateTime = LocalDateTime.now().minusHours(i),
            professions = Seq(
              Fixtures.firstProfessionRef,
              Fixtures.secondProfessionRef
            )
          )
        case i =>
          template.copy(
            dateTime = LocalDateTime.now().minusHours(i),
            professions = Seq(
              Fixtures.thirdProfessionRef
            )
          )
      }
      .toSeq
  }
}
