package care.masana.notes

import care.masana.common.{BeneficiaryRef, IdentityRef, NoteRef, ProfessionRef}

import java.time.LocalDateTime

case class Note(
    ref: NoteRef,
    beneficiary: BeneficiaryRef,
    author: IdentityRef,
    dateTime: LocalDateTime,
    important: Boolean,
    professions: Seq[ProfessionRef]
)
