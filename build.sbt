ThisBuild / scalaVersion := "2.13.8"

// Zio is used in our system as return type for many methods
//libraryDependencies += "dev.zio" %% "zio" % "1.0.14"

// ScalaTest is actually our favorite library for testing
// libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.12" % "test"

lazy val masana = (project in file("."))
  .aggregate(
    common,
    notes
  )

lazy val common = (project in file("masana-common"))

lazy val notes = (project in file("domain/notes"))
  .dependsOn(
    common
  )

lazy val professionals = (project in file("domain/professionals"))
  .dependsOn(
    common
  )

lazy val service = (project in file("masana-service"))
  .dependsOn(
    common,
    professionals,
    notes
  )
